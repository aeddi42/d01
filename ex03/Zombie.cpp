/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:35:07 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 17:27:10 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Zombie.hpp"

void	Zombie::define_zombie(std::string name, std::string type) {

	this->_name = name;
	this->_type = type;

	return;
}

void	Zombie::announce(void) const {

	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss...\n";
}

Zombie::~Zombie(void) {

	std::cout << "Zombie dead for good !\n";
}
