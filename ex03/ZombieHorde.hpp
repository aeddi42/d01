/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 16:18:39 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 17:20:55 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEHORDE_HPP
# define ZOMBIEHORDE_HPP

#include <iostream>
#include "Zombie.hpp"

class ZombieHorde {

public:

	ZombieHorde(int n);
	~ZombieHorde(void);
	void			announce(void) const;

private:

	int				_n;
	Zombie			*_horde;
	std::string		_getRandomName(void) const;

};

#endif /* !ZOMBIEHORDE_HPP */
