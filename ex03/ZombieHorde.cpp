/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 16:18:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 17:27:35 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "Zombie.hpp"
#include "ZombieHorde.hpp"

std::string	ZombieHorde::_getRandomName(void) const {

	char		rand[4];
	int			index;

	std::string	list_name[] = {
		"Thierry",
		"Gustave",
		"Pedro",
		"Martin",
		"Gedeon",
		"Jean-Christophe",
		"Enguerrand",
		"Balthazar",
		"Horacio",
		"Gertrude"
	};

	std::ifstream random("/dev/urandom");
	random.read(rand, 4);
	random.close();

	index = int(*rand);
	index = (index < 0) ? index * -1 : index;
	index %= 10;

	return list_name[index];
}

ZombieHorde::ZombieHorde(int n) {

	int	i;

	this->_n = n;
	this->_horde = new Zombie[n];
	for (i = 0; i < n; i++)
		this->_horde[i].define_zombie(this->_getRandomName(), "basic");

	return;
}

ZombieHorde::~ZombieHorde(void) {

	delete [] this->_horde;
}

void	ZombieHorde::announce(void) const {

	int	i;

	std::cout << std::endl;
	for (i = 0; i < this->_n; i++)
		this->_horde[i].announce();
	std::cout << std::endl;
}
