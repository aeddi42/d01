/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:20:59 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 17:23:56 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

int	main(void) {

	ZombieHorde	*horde = new ZombieHorde(42);
	horde->announce();
	delete horde;

	return 0;
}
