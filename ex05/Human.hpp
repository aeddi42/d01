/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:43:42 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 20:29:03 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
# define HUMAN_HPP

# include "Brain.hpp"
# include <sstream>

class Human {

public:

	const Brain		self;

	const Brain&	getBrain(void);
	std::string		identify(void);

};

#endif /* !HUMAN_HPP */
