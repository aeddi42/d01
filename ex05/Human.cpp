/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:43:37 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 20:29:06 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"
#include "Human.hpp"
#include <sstream>

const Brain&	Human::getBrain(void) {

	return this->self;
}

std::string	Human::identify(void) {

	return this->self.identify();
}
