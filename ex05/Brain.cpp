/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:42:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 12:23:25 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"
#include <sstream>

Brain::Brain(void) {

	this->size = 42;
	this->weight = 420;
}

std::string	Brain::identify(void) const {

	std::stringstream	adress;

	adress << std::hex << this;
	return adress.str();
}
