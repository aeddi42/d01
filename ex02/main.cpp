/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:43:53 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 12:17:19 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int	main(void) {

	int	i;

	ZombieEvent	factory_fast;
	ZombieEvent	factory_big;
	ZombieEvent	factory_armless;
	std::string	name_base = "zombie #";
	std::string	tmp;

	factory_fast.setZombieType("fast");
	factory_big.setZombieType("big");
	factory_armless.setZombieType("armless");

	std::cout << std::endl;
	for (i = 0; i < 3; i++) {
		factory_fast.randomChump();
		factory_big.randomChump();
		factory_armless.randomChump();
	}

	std::cout << std::endl;
	for (i = 0; i < 9; i++) {
		tmp = name_base + std::to_string(i);
		Zombie *heap;
		if (i < 3)
			heap = factory_fast.newZombie(tmp);
		else if (i < 6)
			heap = factory_big.newZombie(tmp);
		else
			heap = factory_armless.newZombie(tmp);
		heap->announce();
		delete heap;
	}
	std::cout << std::endl;

	return 0;
}
