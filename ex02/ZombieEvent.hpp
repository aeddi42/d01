/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:43:47 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 15:36:55 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
# define ZOMBIEEVENT_HPP

# include <iostream>
# include "Zombie.hpp"

class ZombieEvent {

public:

	void	setZombieType(std::string type);
	Zombie	*newZombie(std::string name) const;
	void	randomChump(void) const;

private:

	std::string	_type;

};

#endif /* !ZOMBIEEVENT_HPP */
