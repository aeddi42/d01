/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:43:37 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 12:15:52 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "Zombie.hpp"
#include "ZombieEvent.hpp"

void	ZombieEvent::setZombieType(std::string type) {

	this->_type = type;
	return;
}

Zombie	*ZombieEvent::newZombie(std::string name) const {

	return (new Zombie(name, this->_type));
}

void	ZombieEvent::randomChump(void) const {

	char		rand[4];
	int			index;

	std::string	list_name[] = {
		"Thierry",
		"Gustave",
		"Pedro",
		"Martin",
		"Gedeon",
		"Jean-Christophe",
		"Enguerrand",
		"Balthazar",
		"Horacio",
		"Gertrude"
	};

	std::ifstream random("/dev/urandom");
	random.read(rand, 4);
	random.close();

	index = int(*rand);
	index = (index < 0) ? index * -1 : index;
	index %= 10;

	Zombie stack = Zombie(list_name[index], this->_type);
	stack.announce();

	return;
}
