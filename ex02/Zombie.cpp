/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:35:07 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 15:56:00 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) {

	this->_name = name;
	this->_type = type;

	return;
}

void	Zombie::announce(void) const {

	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss...\n";
}
