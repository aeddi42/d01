/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 13:35:13 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 14:37:35 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

# include <iostream>

class Zombie {

public:

				Zombie(std::string name, std::string type);
	void		announce(void) const;

private:

	std::string	_name;
	std::string	_type;

};

#endif /* !ZOMBIE_HPP */
