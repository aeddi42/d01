/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 13:29:59 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 14:37:54 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP

# include "Weapon.hpp"

class HumanB {

	public:

					HumanB(std::string name);
		void		attack(void);
		void		setWeapon(Weapon & weapon);

	private:

		std::string	_name;
		Weapon *	_weapon;

};

#endif /* !HUMANB_HPP */
