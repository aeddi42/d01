/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 13:29:39 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 14:24:21 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP

# include "Weapon.hpp"

class HumanA {

	public:

					HumanA(std::string name, Weapon & weapon);
		void		attack(void);

	private:

		std::string	_name;
		Weapon &	_weapon;

};

#endif /* !HUMANA_HPP */
