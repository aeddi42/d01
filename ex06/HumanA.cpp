/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 13:29:26 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 14:29:27 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"
#include <iostream>

		HumanA::HumanA(std::string name, Weapon & weapon) : _name(name), _weapon(weapon) {} 

void	HumanA::attack(void) {

	std::cout << this->_name << " attacks with his " << this->_weapon.getType() << std::endl;
}
