/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 13:30:39 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 14:08:31 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

void				Weapon::setType(std::string type) {

	this->_type = type;
}

const std::string&	Weapon::getType(void) {

	return this->_type;
}

					Weapon::Weapon(std::string type) {
	
	this->_type = type;
}
