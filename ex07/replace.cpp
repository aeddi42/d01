/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 17:39:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 17:59:19 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>

void	replaceOccurence(std::ifstream &filein, std::ofstream &fileout, std::string orig, std::string repl) {

	size_t pos;
	std::string	tmp;

	while (getline(filein, tmp))
	{
		pos = 0;
		while ((pos = tmp.find(orig, pos)) != std::string::npos) {
			 tmp.replace(pos, orig.length(), repl);
			 pos += repl.length();
		}
		fileout << tmp << std::endl;
	}
}

int		printUsage(char **av) {

	std::cout << "Usage: " << av[0] << " <Filename> <TextToReplace> <ReplacementText>" << std::endl;
	return 1;
}

int		main(int ac, char **av) {

	std::ifstream filein(av[1]);
	if (!filein)
	{
		std::cout << "Openning file problem" << std::endl;
		return printUsage(av);
	}
	std::ofstream fileout(std::string(av[1]) + ".replace");
	if (!fileout)
	{
		std::cout << "Creating file problem" << std::endl;
		return 1;
	}
	if (ac != 4)
		return printUsage(av);
	replaceOccurence(filein, fileout, std::string(av[2]), std::string(av[3]));
	filein.close();
	fileout.close();

	return 0;
}
