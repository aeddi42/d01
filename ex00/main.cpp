/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 10:42:58 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/07 09:19:54 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Pony.hpp"

Pony*	ponyOnTheHeap(void) {

	Pony *onTheHeap = new Pony("heap");

	onTheHeap->give_a_neigh();
	onTheHeap->give_a_neigh();
	onTheHeap->give_a_neigh();

	std::cout << "Pony on the heap as given " << onTheHeap->given_neighs << " neighs !\n";
	delete onTheHeap;
	return onTheHeap;
}

Pony*	ponyOnTheStack(void) {

	Pony onTheStack = Pony("stack");
	Pony *ptrToTheStack = &onTheStack;

	onTheStack.give_a_neigh();
	onTheStack.give_a_neigh();
	onTheStack.give_a_neigh();

	std::cout << "Pony on the stack as given " << onTheStack.given_neighs << " neighs !\n";
	return ptrToTheStack;
}

int	main(void) {

	Pony *ptrToTheHeap;
	Pony *ptrToTheStack;

	std::cout << "\nPony stories are started !\n\n";
	ptrToTheHeap = ponyOnTheHeap();
	ptrToTheStack = ponyOnTheStack();
	std::cout << "Pony stories are ended !\n\n";

	std::cout << "From the hereafter:\n";
	ptrToTheHeap->give_a_neigh();
	std::cout << "Pony on the heap as given a " << ptrToTheHeap->given_neighs << "th neigh !\n\n";
	std::cout << "From the hereafter:\n";
	ptrToTheStack->give_a_neigh();
	std::cout << "Pony on the stack as given a " << ptrToTheStack->given_neighs << "th neigh !\n\n";
	return 0;
}
