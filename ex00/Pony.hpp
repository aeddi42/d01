/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 10:42:53 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 11:25:41 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
# define PONY_HPP

# include <iostream>

class Pony {

public:

	int				given_neighs;
	std::string		place;

					Pony(std::string place);
					~Pony(void);

	void			give_a_neigh(void);

};

#endif /* !PONY_HPP */
