/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 10:42:29 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/06 11:46:19 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Pony.hpp"

Pony::Pony(std::string place) {

	this->given_neighs = 0;
	this->place = place;
	std::cout << "Pony is borned on the " << this->place << " !\n";
	return;
}

Pony::~Pony(void) {

	std::cout << "Pony is died on the " << this->place << " !\n\n";
	return;
}

void	Pony::give_a_neigh(void) {

	std::cout << "Pony on the " << this->place << ": NEIGH !\n";
	this->given_neighs += 1;
	return;
}
