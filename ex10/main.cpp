/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 23:47:13 by plastic           #+#    #+#             */
/*   Updated: 2015/04/08 14:32:46 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>

int	catFiles(int ac, char **av)
{
	int	index = 0;
	int	ret = 0;
	std::ifstream	infile;
	std::string		stdin_e;

	while (index++ < ac - 1)
	{
		infile.open(av[index]);
		if (!infile.is_open()) {
			std::cout << "cat: " << av[index] << ": No such file or directory" << std::endl;
			ret = 1;
		}
		else {
			while(getline(infile, stdin_e))
				std::cout << stdin_e << std::endl;
			infile.close();
		}
	}
	return ret;
}

int	catStdin(void)
{
	std::string	stdin_e;
	while(getline(std::cin, stdin_e))
		std::cout << stdin_e << std::endl;
	return 0;
}

int main(int ac, char **av)
{
	if (ac > 1)
		return catFiles(ac, av);
	else
		return catStdin();
}
