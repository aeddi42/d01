/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 18:12:01 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/07 23:08:09 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

void	Human::meleeAttack(std::string const & target) {

	std::cout << "Launch melee attach to " << target << std::endl;
}

void	Human::rangedAttack(std::string const & target) {

	std::cout << "Launch ranged attach to " << target << std::endl;
}

void	Human::intimidatingShout(std::string const & target) {

	std::cout << "Launch intimidating shout to " << target << std::endl;
}

void	Human::action(std::string const & action_name, std::string const & target) {

	int						index = 0;
	std::string				actions[3] = {"melee", "ranged", "intimidating"};
	void (Human::*functions[3])(std::string const &) = {&Human::meleeAttack, &Human::rangedAttack, &Human::intimidatingShout};

	while (index < 3)
	{
		if (actions[index] == action_name)
		{
			(this->*functions[index])(target);
			break;
		}
		index++;
	}
}
