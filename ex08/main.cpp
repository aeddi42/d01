/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 23:03:13 by plastic           #+#    #+#             */
/*   Updated: 2015/04/07 23:07:36 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Human.hpp"

int main(void)
{
	Human	human;

	human.action("melee", "Jean-Claude");
	human.action("ranged", "Gerard");
	human.action("intimidating", "Xavier");
	human.action("none", "none");

	return 0;
}
