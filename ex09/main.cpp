/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 23:36:00 by plastic           #+#    #+#             */
/*   Updated: 2015/04/07 23:38:05 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"

int main(void)
{
	Logger	logger("logfile");

	logger.log("console", "Test 1");
	logger.log("file", "Test 2");
	logger.log("console", "Test 3");
	logger.log("file", "Test 4");
	return 0;
}
