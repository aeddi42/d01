/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 23:10:16 by plastic           #+#    #+#             */
/*   Updated: 2015/04/07 23:35:52 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOGGER_HPP
# define LOGGER_HPP

# include <iostream>

class Logger {

	public:

		Logger(std::string const & filename);
		void		log(std::string const & dest, std::string const & message);

	private:

		void		logToConsole(std::string const & log);
		void		logToFile(std::string const & log);
		std::string	makeLogEntry(std::string const & mess);

		std::string	_filename;

};

#endif /* !LOGGER_HPP */
