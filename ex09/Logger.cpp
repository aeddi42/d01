/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/07 23:10:08 by plastic           #+#    #+#             */
/*   Updated: 2015/04/07 23:40:11 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"
#include <fstream>
#include <time.h>

			Logger::Logger(std::string const & filename) : _filename(filename) {}

void		Logger::logToConsole(std::string const & log) {
	std::cout << log << std::endl;
}

void		Logger::logToFile(std::string const & log) {

	std::ofstream outfile;

	outfile.open(this->_filename.c_str(), std::ios_base::app);
	outfile << log << std::endl;
	outfile.close();
}

std::string	Logger::makeLogEntry(std::string const & mess) {
	
	std::string	format;
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [16];

	time(&rawtime);
	timeinfo = localtime (&rawtime);
	strftime(buffer, 16, "%Y%m%d_%H%M%S", timeinfo);

	format = "[" + std::string(buffer) + "] " + mess;
	return format;
}

void		Logger::log(std::string const & dest, std::string const & message) {

	int						index = 0;
	std::string				logs[2] = {"console", "file"};
	void (Logger::*functions[2])(std::string const &) = {&Logger::logToConsole, &Logger::logToFile};

	while (index < 2)
	{
		if (logs[index] == dest)
		{
			(this->*functions[index])(this->makeLogEntry(message));
			break;
		}
		index++;
	}
}
